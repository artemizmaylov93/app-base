%w{ntp mc htop iotop iftop atop vim-common wget curl rkhunter git awstats postfix}.each do |
packages|
    package packages do
    action :install
    end
end
